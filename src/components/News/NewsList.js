import React, { Component } from 'react';
import NewsComponent from './NewsComponent';

class NewsList extends Component {
  render() {
    return (
      <section className="NewsList">
        <header className="text-center"><h1>NIEUWS</h1></header>
        <NewsComponent
          title="Nieuwe naam boeddhistische tv bij de KRO/NCRV"
          date="15-09"
          content="Onder de naam De Boeddhistische Blik gaat KRO/NCRV vanaf 17 september verder met het uitzenden van boeddhistische programma’s." />
        <NewsComponent
          title="Ook de balen van de angstcultuur? Doe mee met een experiment voor tv"
          date="13-10"/>
      </section>
    );
  }
}

export default NewsList;

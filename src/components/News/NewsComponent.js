import React, { Component } from 'react';

class NewsComponent extends Component {

  render() {
    return (
      <a className="NewsComponent">
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-9">
            <h2>{this.props.title}</h2>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-3">
            <em className="news-date text-muted">{this.props.date}</em>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <div className="text-muted">{this.props.content}</div>
          </div>
        </div>
      </a>
    );
  }
};

export default NewsComponent;

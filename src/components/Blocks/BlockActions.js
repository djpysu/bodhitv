import React, { Component } from 'react';

class BlockActions extends Component {

  render() {
    return (
      <div className="icon-actions">
        <a href="/" title="Add To Wishlist"><i className="fa fa-heart-o" aria-hidden="true"> </i></a>
        <a href="/" title="Share article"><i className="fa fa-share" aria-hidden="true"> </i></a>
      </div>
    );
  }
};

export default BlockActions;

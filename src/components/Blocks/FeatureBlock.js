import React, { Component } from 'react';
import BlockActions from './BlockActions';

class FeatureBlock extends Component {

  render() {
    return (
      <article className="FeatureBlock">
          <img className="img-responsive img-background" src = {this.props.imageBackground} alt = {this.props.title} />
          <div className="block-content">
            <h1 className="title">{this.props.category}</h1>
            <p className="m-t-10"><strong>{this.props.title}</strong></p>
            <p>{this.props.content}</p>
            <div className="section-actions">
                <small className="m-r-5"><i className="fa fa-clock-o" aria-hidden="true"></i> 01-09-2017</small>
                <small><i className="fa fa-tags" aria-hidden="true"></i> buddhabites, leraar</small>
            </div>
          </div>
          <BlockActions />
      </article>
    );
  }
};

export default FeatureBlock;

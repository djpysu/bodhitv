import React, { Component } from 'react';
import TextTruncate from 'react-text-truncate';
import BlockActions from './BlockActions';

class SimpleBlock extends Component {

  render() {

    const videoType = this.props.type;
    let middleIcon = null;
    if (videoType === 'video') {
      middleIcon = <a href="/" className="middle-icon"><i className="fa fa-play" aria-hidden="true"></i></a>
    }

    return (
      <article className={`SimpleBlock ${this.props.type}`}>
        <div className="image-container">
         <img className="img-responsive img-background" src = {this.props.imageBackground} alt = {this.props.title}/>
         {middleIcon}
        </div>
        <div className="block-content">
          <h3 className="title">{this.props.title}</h3>
          <TextTruncate className="text-muted"
              line={this.props.lines}
              truncateText="…"
              text={this.props.content}
          />
        </div>
        <BlockActions />
      </article>
    );
  }
};

export default SimpleBlock;

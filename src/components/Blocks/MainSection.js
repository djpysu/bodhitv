import React, { Component } from 'react';
import {Carousel} from 'react-bootstrap';
import slide1 from '../../images/b1.jpg';
import slide2 from '../../images/b2.jpg';

class MainSection extends Component {
  render() {
    return (
      <section className="MainSection">
        <Carousel>
          <Carousel.Item>
            <a href="/" title="Films die je niet mag missen tijdens het BFFE">
              <img width={900} height={500} alt="Films die je niet mag missen tijdens het BFFE" src={slide1}/>
              <Carousel.Caption>
                <h2 className="title">Featured Article</h2>
                <p className="m-t-10"><strong>Films die je niet mag missen tijdens BFFE</strong></p>
                <p className="carousel-description">Ook dit jaar vindt weer het Buddhist Filmfestival Europe plaats in filmmuseum Eye in Amsterdam.
                 Wat kan je als bezoeker verwachten? Bodhitv maakte alvast een selectie van vijf films uit het programma</p>
                <div className="section-actions">
                    <small className="m-r-5"><i className="fa fa-list" aria-hidden="true"></i> Artikelen</small>
                    <small><i className="fa fa-clock-o" aria-hidden="true"></i> 54m ago</small>
                </div>
              </Carousel.Caption>
            </a>
          </Carousel.Item>
          <Carousel.Item>
            <a href="/" title="Boeddhisme en rockmuziek">
              <img width={900} height={500} alt="Boeddhisme en rockmuziek" src={slide2}/>
              <Carousel.Caption>
                <h2 className="title">Featured Video</h2>
                <p className="m-t-10"><strong>Boeddhisme en rockmuziek</strong></p>
                <p className="carousel-description">Bowie, Beastie Boys en Boeddha. Een lijst met pop en rockmuziek die zich bezighoudt met boeddhisme. Heb je zelf een favoriet? Laat het dan weten.</p>
                <div className="section-actions">
                    <small className="m-r-5"><i className="fa fa-list" aria-hidden="true"></i> Videos</small>
                    <small><i className="fa fa-clock-o" aria-hidden="true"></i> 32m ago</small>
                </div>
              </Carousel.Caption>
            </a>
          </Carousel.Item>
        </Carousel>
      </section>
    );
  }
}

export default MainSection;

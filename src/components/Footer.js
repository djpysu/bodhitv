import React, { Component } from 'react';

class Footer extends Component {

  render() {

    return (
      <section className="FooterContainer">
        <div className="Footer m-t-60">
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-sm-2">
                <h4 className="title">EXPLORE BODHITV</h4>
              </div>
              <div className="col-xs-12 col-sm-3 col-sm-offset-1">
                <ul>
                  <li><a href="/">Over Bodhitv</a></li>
                  <li><a href="/">Nieuwsbrief, Facebook, Twitter, RSS</a></li>
                  <li><a href="/">Contact</a></li>
                  <li><a href="/">Colofon</a></li>
                  <li><a href="/">Algemene voorwaarden</a></li>
                  <li><a href="/">Privacy policy</a></li>
                </ul>
              </div>
              <div className="col-xs-12 col-sm-3">
                <ul>
                  <li><a href="/">Login</a></li>
                  <li><a href="/">Wachtwoord vergeten</a></li>
                  <li><a href="/">Registreren</a></li>
                </ul>
              </div>
              <div className="col-xs-12 col-sm-3">
                <ul>
                  <li><a href="/">Home</a></li>
                  <li><a href="/">Bodhitv</a></li>
                  <li><a href="/">Artikelen</a></li>
                  <li><a href="/">Nieuws</a></li>
                  <li><a href="/">Agenda</a></li>
                  <li><a href="/">Algemeen</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="FooterCopy">
          <div className="container">
            <div className="row">
              <div className="col-xs-12">
                <a href="https://www.facebook.com" target="_blank"><i className="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="https://www.twitter.com" target="_blank"><i className="fa fa-twitter" aria-hidden="true"></i></a>
                <p className="text-muted inline">Copyright (C) 2017 - Bodhittv.nl - Deze website, de inhoud en rechten zijn eigendom van de Stichting Vrienden van de BOS</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
};

export default Footer;

import React, { Component } from 'react';
import {Nav, Navbar, NavItem, Button} from 'react-bootstrap';
import logo from '../images/logo-bodhi.png';

class Navs extends Component {

  render() {

    return (
      <Navbar collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/"><img src={logo} className="logo" alt="logo" /></a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <NavItem eventKey={1} href="/">Videos</NavItem>
            <NavItem eventKey={2} href="/">Artikelen</NavItem>
            <NavItem eventKey={2} href="/">Nieuws</NavItem>
            <NavItem eventKey={2} href="/">Agenda</NavItem>
            <NavItem eventKey={2} href="/">Algemeen</NavItem>
          </Nav>
          <Nav pullRight>
            <Button bsStyle="primary">Login</Button>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
};

export default Navs;

import React, { Component } from 'react';
import { Router } from 'react-router';
import { Route, BrowserRouter} from 'react-router-dom';
import './css/App.css';

/* images */
import feature1 from './images/videos.jpg';
import feature2 from './images/artikelen.jpg';
import feature3 from './images/videos2.jpg';
import art1 from './images/art1.jpg';
import art2 from './images/art2.jpg';
import art3 from './images/art3.jpg';
import art4 from './images/art4.jpg';
import vid1 from './images/vid1.jpg';
import vid2 from './images/vid2.jpg';
import vid3 from './images/vid3.jpg';
import vid4 from './images/vid4.jpg';
import vid5 from './images/vid5.jpg';
/* components */
import Navs from './components/Navs';
import MainSection from './components/Blocks/MainSection';
import NewsList from './components/News/NewsList';
import FeatureBlock from './components/Blocks/FeatureBlock';
import SimpleBlock from './components/Blocks/SimpleBlock';
import Footer from './components/Footer';
import Video from './components/Videos/Video';


const Page = ({ title }) => (
    <div className="App">
      <Navs />
      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-8">
            <MainSection />
          </div>
          <div className="col-xs-12 col-sm-12 col-md-4">
            <NewsList />
          </div>
        </div>
        <section>
          <div className="row m-t-30">
            <div className="col-xs-12 col-md-4">
              <FeatureBlock imageBackground={feature1} category="Videos" title="Een goede boeddhistisch leraar?" content="Veel mensen die met het boeddhisme bezig zijn komen vroeger of later in aanraking met een leraar."/>
            </div>
            <div className="col-xs-12 col-md-4">
              <FeatureBlock imageBackground={feature2} category="Artikelen" title="TV: In de schoot van de zee" content="Op het strand van Scheveningen worden een jaar rond enkele mensen gevolgd die troost en bezinning vinden bij de zee."/>
            </div>
            <div className="col-xs-12 col-md-4">
              <FeatureBlock imageBackground={feature3} category="Videos" title="Liefdesroep - Hannah Cuppen" content="n haar nieuwe boek 'Liefdesroep' pleit  Hannah Cuppen voor het kiezen voor de roep van je hart."/>
            </div>
          </div>
        </section>
        <section>
          <div className="row m-t-60">
            <div className="col-xs-12"><h2 className="title">Artikelen</h2></div>
          </div>
          <div className="row m-t-30">
            <div className="col-xs-6 col-sm-3">
              <SimpleBlock imageBackground = {art1} title = "Buddhist Film Festival Europe zoekt vrijwilligers voor 2017" content="Het Buddhist Film Festival Europe is klein maar fijn, en wordt georganiseerd door een zeer klein groepje gedreven mensen. Het festival zoekt enthousiaste vrijwilligers voor de editie van 2017." lines="4"/>
            </div>
            <div className="col-xs-6 col-sm-3">
              <SimpleBlock imageBackground = {art2} title = "Dossier Birma (Myanmar)" content = "Birma is al jaren een toneel van strijd tussen verschillende groeperingen, waaronder ook een grote groep boeddhistische monniken. In dit dossier vind je alle artikelen en documentaires over dit land op een rij." lines="4"/>
            </div>
            <div className="col-xs-6 col-sm-3">
              <SimpleBlock imageBackground = {art3} title = "TV: De macht van monniken in een nieuw Myanmar" content = "Sinds de verkiezing van de partij van Aung San Suu Kyi is het democratische Birma op zoek naar de eigen identiteit. Boeddhistische monniken hebben geen stemrecht maar wel grote maatschappelijke invloed. Zij vragen zich af hoe Birma er in de toekomst uit gaat zien. Zondag 17 sept op NPO2." lines="4"/>
            </div>
            <div className="col-xs-6 col-sm-3">
              <SimpleBlock imageBackground = {art4} title = "Buddhist Film Festival Europe 2017" content = "Het programma van het 12e Buddhist Film Festival Europe, dat loopt van 29 september tot en met 1 oktober in EYE in Amsterdam is bekend. De openingsfilm is 'The Last Dalai Lama?' van regisseur Mickey Lemle." lines="4"/>
            </div>
          </div>
        </section>
        <section>
          <div className="row m-t-60">
            <div className="col-xs-12"><h2 className="title">Videos</h2></div>
          </div>
          <div className="row m-t-30">
            <div className="col-xs-12 col-sm-6">
              <SimpleBlock type="video" imageBackground = {vid1} title = "Ontspannen op vakantie?" content = "Je kent het: inpakstress, paspoort vergeten, het vliegveld is een hel. Hoe ga je in hemelsnaam ontspannen op vakantie? Misha Beliën vraagt het reizigers, en 'stressontknoper' en zenboeddhist Paul Loomans."/>
            </div>
            <div className="col-xs-12 col-sm-6">
              <div className="row">
                <div className="col-xs-12 col-sm-6">
                  <SimpleBlock imageBackground = {vid2} title = "Zelfpluktuin op Schiermonnikoog"  content= "Als klinisch psycholoog zei Janneke van der Velde vaak tegen cliënten: volg je hart. Toen ze dat zelf ook ging doen, was dat het begin van de zelfpluktuin op Schiermonnikoog." lines="1"/>
                </div>
                <div className="col-xs-12 col-sm-6">
                  <SimpleBlock imageBackground = {vid3} title = "Buddha Bites: Formatie 2017"  content= "Wat kan het boeddhisme betekenen voor de kabinetsformatie? Met die vraag waagt Misha Beliën zich op het Binnenhof, waar hij Buma, Rutte, Pechtold en Klaver aan de mouw trekt." lines="1"/>
                </div>
              </div>
              <div className="row m-t-35">
                <div className="col-xs-12 col-sm-6">
                  <SimpleBlock imageBackground = {vid4} title = "Rob Birza: Kijken zonder oordeel" content= "In India wordt alles symbolisch opgevat, daar is geen ontkomen aan. Wat symboliseert de hangende slang in de 3,5 meter hoge en 7 meter lange sculptuur van kunstenaar Rob Birza?" lines="1" />
                </div>
                <div className="col-xs-12 col-sm-6">
                  <SimpleBlock imageBackground = {vid5} title = "Rondom sterven - Irene de Oude 2" content= "Volgens Irene de Oude kijken wij als samenleving steeds meer naar ouderen als een kostenpost. Ze pleit voor een aanpak waarin we juist de potentie van ouderen aanspreken." lines="1"/>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </div>
);

const Home = (props) => (
  <Page title="Home"/>
);

// const Video = (props) => (
//   <Page title="Videos"/>
// );

class App extends Component {
  render() {
    return (
        <div>
          <Route exact path="/" component={Home}/>
          <Route exact path="/videos" component={Video}/>
        </div>
    );
  }
}

export default App;
